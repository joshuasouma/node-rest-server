const jwt = require('jsonwebtoken');


//================
//Verificar token
//================
let verificaToken = (req, res, next) => {

    let token = req.get('auth');

    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'token no válido'
                }
            });
        }

        req.usuario = decoded.usuario;

        next();
    });



};

//================
//Verificar token para imagenes
//================
let verificaTokenImg = (req, res, next) => {
    let token = req.query.token;

    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'token no válido'
                }
            });
        }

        req.usuario = decoded.usuario;

        next();
    });

}

//================
//Verificar ROL ADMIN
//================

let verificaAdminRol = (req, res, next) => {
    let usuario = req.usuario;

    if (usuario.role !== 'ADMIN_ROLE') {
        return res.json({
            ok: false,
            err: {
                message: 'El usuario no es admin',
            }
        });
    } else {
        next();
    }




};

module.exports = {
    verificaToken,
    verificaTokenImg,
    verificaAdminRol
}