const express = require('express');

const app = express();



app.use(require('../routes/usuario.js'));
app.use(require('../routes/login.js'));
app.use(require('../routes/categoria.js'));
app.use(require('../routes/producto.js'));
app.use(require('../routes/upload.js'));
app.use(require('../routes/imagen.js'));





module.exports = app;