const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();

const Usuario = require('../models/usuario');
const Producto = require('../models/producto');

const fs = require('fs');
const path = require('path');

//default options
app.use(fileUpload({ useTempFiles: true }));

app.put('/upload/:tipo/:id', async(req, res) => {

    let tipo = req.params.tipo;
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'No se seleccionó ningún archivo'
            }
        });
    }

    //validar tipo
    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'los tipos permitidos son ' + tiposValidos.join(', '),
                tipo
            }
        });
    }

    let archivo = req.files.archivo;
    let nombreArchivoSplitted = archivo.name.split('.');
    console.log(nombreArchivoSplitted);
    let extension = nombreArchivoSplitted[nombreArchivoSplitted.length - 1];

    //Extensiones permitidas
    let extensionesValidas = ['png', 'jpg', 'jpeg', 'gif'];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({
            ok: false,
            err: {
                message: 'Solo archivos ' + extensionesValidas.join(','),
                extension
            }
        });
    }

    //cambiar nombre a archivo
    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;



    await archivo.mv(`uploads/${tipo}/${nombreArchivo}`)
        .then(_ => {
            if (tipo === 'usuarios') {
                imagenUsuario(id, res, nombreArchivo);
            } else {
                imagenProducto(id, res, nombreArchivo);
            }

        })
        .catch(err => res.status(500).json({
            ok: false,
            err
        }));


});

function imagenUsuario(id, res, nombreArchivo) {
    Usuario.findByIdAndUpdate(id, { img: nombreArchivo }, (err, usuarioDB) => {
        if (err) {
            borraArchivo(nombreArchivo, 'usuarios');

            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!usuarioDB) {
            borraArchivo(nombreArchivo, 'usuarios');

            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El usuario no existe'
                }
            });
        }

        borraArchivo(usuarioDB.img, 'usuarios');

        res.json({
            ok: true,
            usuario: usuarioDB,
            img: nombreArchivo
        });


    });
}

function imagenProducto(id, res, nombreArchivo) {
    Producto.findByIdAndUpdate(id, { img: nombreArchivo }, (err, productoDB) => {
        if (err) {
            borraArchivo(nombreArchivo, 'productos');

            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productoDB) {
            borraArchivo(nombreArchivo, 'productos');

            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El producto no existe'
                }
            });
        }

        borraArchivo(productoDB.img, 'productos');

        res.json({
            ok: true,
            producto: productoDB,
            img: nombreArchivo
        });


    });
}

function borraArchivo(nombreImg, tipo) {
    let pathImg = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImg}`);
    console.log(pathImg);

    if (fs.existsSync(pathImg)) {
        fs.unlinkSync(pathImg);
    }
}

module.exports = app;