const express = require('express');

let { verificaToken, verificaAdminRol } = require('../middlewares/authentication');

let app = express();

let Categoria = require('../models/categoria');

//============================
//Mostrar todas las categorias
//============================

app.get('/categoria', (req, res) => {
    Categoria.find({})
        .sort('descripcion')
        .populate('usuario', 'nombre email')
        .exec((err, categorias) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if (!categorias) {
                return res.json({
                    ok: true,
                    message: 'No hay categorías que mostrar'
                });
            }

            res.json({
                ok: true,
                categorias
            });

        });
});

//============================
//Mostrar todas las categoria por ID
//============================

app.get('/categoria/:id', (req, res) => {
    //Categoria.findByID();
    let id = req.params.id;
    Categoria.findById(id, (err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err: "No se encontró la categoría",
            });
        }

        res.json({
            ok: true,
            categoriaDB
        });

    });
});

//============================
//Crear nueva categoria
//============================

app.post('/categoria', verificaToken, (req, res) => {
    //regresa la nueva categoria
    let body = req.body;

    let categoria = new Categoria({
        descripcion: body.descripcion,
        usuario: req.usuario._id,
    });
    categoria.save((err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            categoria: categoriaDB
        });
    });

});

//============================
//Crear nueva categoria
//============================

app.put('/categoria/:id', verificaToken, (req, res) => {
    //Actualizar el nombre de la categoria
    let id = req.params.id;
    let descripcion = {
        descripcion: req.body.descripcion
    };
    Categoria.findByIdAndUpdate(id, descripcion, { new: true, runValidators: true, context: 'query' },
        (err, categoriaDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err

                });
            }
            if (!categoriaDB) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                categoria: categoriaDB
            });

        });

});

app.delete('/categoria/:id', [verificaToken, verificaAdminRol], (req, res) => {
    let id = req.params.id;

    Categoria.findByIdAndRemove(id, (err, categoriaDB) => {
        if (err) {

            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err: 'El id no existe'
            });
        }

        res.json({
            ok: true,
            categoria: categoriaDB
        });
    });

});



module.exports = app;