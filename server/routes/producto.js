const express = require('express');


const { verificaToken } = require('../middlewares/authentication');

let app = express();
let Producto = require('../models/producto');


//=========================
// Obtener productos
//=========================
app.get('/productos', (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);
    let limite = req.query.limite || 5;
    limite = Number(limite);

    Producto
        .find({ disponible: true }, 'nombre descripcion precioUni disponible')
        .skip(desde)
        .limit(limite)
        .sort('nombre')
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .exec((err, productos) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if (!productos) {
                return res.status(200).json({
                    ok: true,
                    message: 'No hay productos'
                });
            }

            res.json({
                ok: true,
                productos
            })
        });


});

//=========================
// Obtener producto por ID
//=========================
app.get('/productos/:id', async(req, res) => {

    let id = req.params.id;

    // Producto.findById(id, (err, productoDB) => {
    //     if (err) {
    //         return res.status(500).json({
    //             ok: false,
    //             err
    //         });
    //     }
    //     if (!productoDB) {
    //         return res.status(400).json({
    //             ok: false,
    //             err: 'No se encontró ningún producto'
    //         });
    //     }

    //     res.json({
    //         ok: true,
    //         producto: productoDB
    //     })

    // });
    await Producto.findById(id)
        .then(productoDB => res.json({
            ok: true,
            producto: productoDB || 'No se encontraron productos'
        }))
        .catch(err => res.status(500).json({
            ok: false,
            err
        }));



    //populate: Usuario categoria

});


//=========================
// Buscar Producto
//=========================
app.get('/productos/buscar/:termino', verificaToken, async(req, res) => {

    let termino = req.params.termino;

    let regex = new RegExp(termino, 'i');

    await
    Producto
        .find({ nombre: regex })
        .populate('categoria', 'descripcion')
        .exec()
        .then(productos => res.json({
            ok: true,
            productos
        }))
        .catch(err => res.status(500).json({
            ok: false,
            err
        }));

});



//=========================
// crear producto
//=========================
app.post('/productos', verificaToken, (req, res) => {
    //grabar el usuario
    //grabar una categoria del listado
    let body = req.body;
    let producto = new Producto({
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        disponible: body.disponible,
        categoria: body.categoria,
        usuario: req.usuario._id,
    });
    producto.save((err, productoDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!productoDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        })

    });




});

//=========================
// Actualizar producto
//=========================
app.put('/productos/:id', verificaToken, (req, res) => {

    let id = req.params.id
    let body = req.body;

    Producto.findByIdAndUpdate(
        id,
        body, { new: true, runValidators: true, context: 'query' },
        (err, productoDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if (!productoDB) {
                return res.status(400).json({
                    ok: false,
                    err: 'No se encontró el producto'
                });
            }

            res.json({
                ok: true,
                producto: productoDB
            })
        }
    );


});

//=========================
// Eliminar producto
//=========================
app.delete('/productos/:id', verificaToken, (req, res) => {
    //grabar el usuario
    //grabar una categoria del listado
    //desabilitar
    let id = req.params.id


    Producto.findByIdAndUpdate(
        id, { disponible: false }, { new: true, runValidators: true, context: 'query' },
        (err, productoDB) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if (!productoDB) {
                return res.status(400).json({
                    ok: false,
                    err: 'No se encontró el producto'
                });
            }

            res.json({
                ok: true,
                producto: productoDB
            })
        }
    );




});





module.exports = app;